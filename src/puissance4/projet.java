package puissance4;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class projet {
	//Grille de jeu, valeurs de base à 0
	static int[][] grid;
	//Pour ajouter de la couleur
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_YELLOW = "\u001B[33m";


	public static void initialiseGrille() {
		grid = new int[6][7];
		//Valeurs de base à 0, au cas ou
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < grid[i].length; j++){
				grid[i][j] = 0;
			}
		}
	}
	public static void afficheGrille() {
		/*Affiche la grille dans la console */
		for(int i = grid.length-1; 0 <= i; i--){
			System.out.print("|"); // Grille du début de chaque ligne, nombre de lignes grid.length+1
			for(int j = 0; j < grid[i].length; j++){
				String c = "";
				if(grid[i][j] == 0){
					c += " ";
				}
				else if(grid[i][j] == 1) {
					c += ANSI_YELLOW + "X"; //Joueur 1 => Jaune
				}
				else {
					c += ANSI_RED + "O"; // Joueur 2 => rouge
				}
				System.out.print(c + ANSI_RESET + "|");
			}
			System.out.println("");
		}
		//Dernière ligne affichant les numéros de collone
		for(int i = 0; i < grid[0].length; i++){
			System.out.print(" " + i);
		}
		System.out.println(" ");
	}

	/*
	 Fonctions coup random - Pour l'ia 
	*/
	public static int entierAleatoire(int a, int b){
		//Retourne un entier aléatoire entre a (inclus) et b (inclus)
		return ThreadLocalRandom.current().nextInt(a, b + 1);	
	}
	public static boolean coupRandom(){
		/* Joue pour l'ia : une colonne aléatoire */
		int ligne = -1;
		int colonne = -1;
		while(ligne == -1){
			//Si pas pauvaise ligne
			colonne = entierAleatoire(0, grid.length);
			ligne = getLigne(colonne);
			if(ligne != -1){
				grid[ligne][colonne] = 2;
				return aGagne(2, ligne, colonne);
			}
		}
		return false; //Normalement jamais executé
	}
	public static boolean coupRandom2(){
		return victoireRapide(false); 
		/*La fonction qui décris le comportement est dans cette fonction est dans cette fonction,
		vu que je la re-utilise ça évite de dupliquer du code, paramètre false pour dire vrai/faux pour activer coupRandomAmeliore (niveau 3) */
	}
	public static boolean coupRandom3(){
		return victoireRapide(true);
	}
	public static boolean victoireRapide(boolean niveau3){
		/* Joue pour l'ia et teste si elle peut gagner un un coup*/
		int ligne = -1;
		//Condition ligne = -1, au cas ou si elle serait pleine !
		for(int colonne = 0; colonne < grid[0].length; colonne++){ //je mets k car i déja utilisé
			ligne = getLigne(colonne);
			if(ligne != -1 && aGagne(2, ligne, colonne)){ //Utilisation du "&&" pour empecher l'erreur out of range
			//Stratégie coup 2 => victoire rapide
				grid[ligne][colonne] = 2;
				return true; //L'ia a gagné, test fait juste en haut donc inutile de dupliquer
			}
		}
		//Si aucune case trouvée en victoire rapide
		if(niveau3) return empecherVictoire(); //Si on demande le coup random amélioré, retourner sa fonction
		else return coupRandom(); //Pour coupRandom2	
	}
	public static boolean empecherVictoire(){
		/* Regarde si le joueur 1 peut gagner en un coup, la stratégie est de l'empecher de gagner en prennant sa place*/
		int ligne = -1;
		for(int colonne = 0; colonne < grid[0].length; colonne++){
			ligne = getLigne(colonne);
			if(ligne != -1 && aGagne(1, ligne, colonne)){ //Utilisation du "&&" pour empecher l'erreur out of range
				grid[ligne][colonne] = 2;
				return aGagne(2, ligne, colonne);
			}
		}
		return coupRandomAmeliore(); //Si il trouve aucune case à empecher
	}
	public static boolean coupRandomAmeliore(){
		/* Vérifie si on place un jeton sur une case, si pour le coup d'après le joueur peut gagner => h+1*/
		int ligne = -1;
		int colonne = -1;
		while(ligne == -1){
			//Si pas pauvaise ligne
			colonne = entierAleatoire(0, grid.length);
			ligne = getLigne(colonne); //Vérifie si on peux jouer sur cette colonne
			if(ligne != -1 && (ligne+1 >= grid.length || !aGagne(1, ligne+1, colonne))){ //Regarde si le joueur gagne si il place son jeton au-dessus
				grid[ligne][colonne] = 2;
				return aGagne(2, ligne, colonne);
			}
			else ligne = -1;
		}
		return false; //jamais exécuté normalement
	}


	/* 
		Fonctions pour vérifier les conditions de victoire
	*/
	public static boolean aGagneHor(int joueur, int i, int j){
		/*
			Vérifie si un joueur a gagné sur l'horizontal (gauche-droite)
		*/
		int k = 1; //Compteur, k = 1 en comptant la case actuelle
		//Gauche
		int l = j-1;
		while(0 <= l && grid[i][l] == joueur){
			k++;
			l--;
		}
		//Droite
		l = j+1;
		while(l < grid[i].length && grid[i][l] == joueur){
			k++;
			l++;
		}
		return k == 4;
	}
	public static boolean aGagneVer(int joueur, int i, int j){
		/*
			Vérifie si un joueur a gagné sur l'horizontal (haut-bas)
		*/
		int k = 1; //Compteur, k = 1 en comptant la case actuelle
		//Haut
		int l = i-1;
		while(0 <= l && grid[l][j] == joueur){
			k++;
			l--;
		}
		//Bas
		l = i+1;
		while(l < grid.length && grid[l][j] == joueur){
			k++;
			l++;
		}
		return k == 4;
	}
	public static boolean aGagneDiagMont(int joueur, int i, int j){
		/*
			Vérifie si un joueur a gagné sur la diagonale 1(bas-gauche => haut-droit)
		*/
		int k = 1;

		//Bas gauche
		int m = i-1;
		int l = j-1;
		while(0 <= l && 0 <= m && grid[m][l] == joueur){
			k++;
			l--;
			m--;
		}
		//Haut droite
		m = i+1;
		l = j+1;
		while(m < grid.length && l < grid[m].length && grid[m][l] == joueur){
			k++;
			m++;
			l++;
		}
		return k == 4;
	}
	public static boolean aGagneDiagDesc(int joueur, int i, int j){
		/*
			Vérifie si un joueur a gagné sur la diagonale 1(haut-gauche => bas-droit) 
		*/
		int k = 1;
		//Bas gauche
		int m = i+1;
		int l = j-1;
		while(0 <= l && m < grid.length && grid[m][l] == joueur){
			k++;
			l--;
			m++;
		}
		//Haut droite
		m = i-1;
		l = j+1;
		while(0 <= m && l < grid[m].length && grid[m][l] == joueur){
			k++;
			m--;
			l++;
		}
		return k == 4;
	}
	public static boolean aGagne(int joueur, int i, int j){
		//Pour toutes les diagonales
		return aGagneHor(joueur, i, j) || aGagneVer(joueur, i, j) || aGagneDiagMont(joueur, i, j) || aGagneDiagDesc(joueur, i, j);
	}
	public static boolean matchNul(){
		/*
			Vérifie si la grille est pleine => aucune case qui est égal à 0
		*/
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < grid[i].length; j++){
				if(grid[i][j] == 0) return false;
			}
		}
		return true;
	}
	/*
		Fonctions pour le déroulage du jeu
	*/
	
	public static int choisirColonne(){
		//Ouvre le scanner et attends un entier
		Scanner sc = new Scanner(System.in);
		int colonne = sc.nextInt();
		// sc.close(); J'y arrive pas sans fermer le scanner
		return colonne;
	}
	public static int jouer(int joueur){
		/*
			Boucle éxécutée à chaque tour
		*/
		int ligne = -1;
		while(ligne == -1)
		{
			String color;
			if(joueur == 1){
				color = ANSI_YELLOW;
			}
			else {
				color = ANSI_RED;
			}
			System.out.println(color + "C'est au tour du joueur " + joueur + " de jouer" + ANSI_RESET);
			//Coordonnées de l'endroit visé
			//int colonne = sc.nextInt() => erreur si on tape des caractères
			int colonne = choisirColonne();
			ligne = getLigne(colonne);
			if(ligne != -1){
				//Si pas pauvaise ligne
				grid[ligne][colonne] = joueur;
				if(aGagne(joueur, ligne, colonne)){
					return joueur;
				}
				else{
					return -1;
				} //Retourne le numéro du joueur gagné ou -1(continue a jouer)
				 //Condition de fin
			}
			else{
				System.out.println("Cette collone est pleine ou est en dehors du jeu !");
			}
		}
		return -1;
	}
	public static int getLigne(int colonne){
		/* 
			A partir d'une colonne, récupère l'endroit ou devrait être le prochain jeton.
			Si colonne pleine, renvoie -1, sinon l'indice de la ligne ou poser l'élément.
		*/
		if(colonne < 0 || colonne >= grid[0].length) return -1; //Si en dehors de la grille
		int i = 0;
		while(i < grid.length && grid[i][colonne] != 0){
			//Descend le jeton dans la grille tant que la case est pas remplie
			i++;
		}
		if(i < grid.length){
			return i;
		}
		else{
			//Pas possible en dehors de la grille !
			return -1;
		}
	}
	public static void jeu(boolean ia){
		/*
			Boucle principale du jeu
		*/
		initialiseGrille();
		int fin = -1;
		int joueur = 1;
		while(fin == -1){
			//Boucle tant que pas gagné/perdu/égalité pour chaque coup joué par un joueur
			if(!matchNul()){
				afficheGrille();
				fin = jouer(joueur);
			}
			else fin = 0; //Si grille pleine, la fin = 0

			if(!ia) joueur = joueur == 1 ? 2 : 1; //Si il y a pas d'ia, on fait le changement de joueur, switch entre 2 et 1
			else if(!matchNul() && fin == -1 && coupRandom3()) fin = 2;
		}
		afficheGrille(); //Affiche une dernière fois
		/* Codes de fin:
			fin = -1 => pas fini, le jeu continue
			fin = 1, le joueur 1 a gagné
			fin = 2, le joueur 2 à gagné(ou l'ia)
			sinon, c'est que fin = 0 => Aucun joueur a gagné car la grille est pleine
		*/
		if(fin == 1){
			System.out.println(ANSI_YELLOW + "Le joueur 1 à gagné !" + ANSI_RESET);
		}
		else if(fin == 2){
			System.out.println(ANSI_RED + "Le joueur 2 à gagné !" + ANSI_RESET);
		}
		else{
			System.out.println("Aucun joueur à gagné");
		}
	}
	public static void main(String[] args) {
		boolean ia_res = false;
		boolean ia = false;
		while(!ia_res){ //tant qu'on a pas choisi l'ia
			System.out.println("Souhaitez vous jouer contre l'ia (y/n)");
			Scanner sc = new Scanner(System.in);
			String res = sc.next(); //récupère le texte depuis la console
			//sc.close() //j'arrive pas a fermer le scanner aussi
			if(res.equals("y")) {ia = true; ia_res = true;}
		 	if(res.equals("n")) { ia = false; ia_res = true;}
		}
		jeu(ia);
	}
}
